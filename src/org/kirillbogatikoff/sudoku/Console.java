package org.kirillbogatikoff.sudoku;

public class Console {
	public static void main(String[] args) {
		Generator gen = new Generator();
		Map map = gen.generate();
		map = gen.generateHoles(30, map);
		System.out.println(map);
	}
}
