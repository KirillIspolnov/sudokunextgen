package org.kirillbogatikoff.sudoku;

import java.util.Arrays;
import java.util.StringJoiner;

/**
 * Представляет часть поля Судоку:
 * строку, столбец или блок
 * Содержит методы, облегчающие
 * взаимодествие с картой Судоку:
 * добавление новых чисел, проверка вхождений, 
 * преобразование в массив или строку 
 * 
 * В дальнейшем любая из частей карты Судоку будет
 * именоваться контейнером
 * 
 * @author Кирилл И., 16ит18К
 * @version 0.1.4
 */
public class Part {
	/* числа в строке */
	private int[] numbers;
	/* текущая позиция. используется при добавлении 
	 * нового числа */
	private int pointer = 0;
	
	/**
	 * Инициализирует контейнер нужной длины <code>size</code>
	 * 
	 * @param size размерность судоку (длина строки или столбца)
	 */
	public Part(int size) {
		this.numbers = new int[size];
	}
	
	/**
	 * Определяет наличие числа в контейнере
	 * Следуя правилам Судоку, числа в контейнере
	 * не могут повторяться
	 * 
	 * @param target число, наличие которого в контейнере и проверяется
	 * @return true, если контейнер уже содержит данное число, false во всех остальных случаях
	 */
	public boolean has(int target) {
		//перебор элементов
		for(int number : numbers) {
			if(number == target) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Добавляет новое число в контейнер,
	 * если такого числа еще нет в контейнере
	 * 
	 * @param number число, которое следует добавить в контейнер
	 * @return true, если число добавлено в контейнер, false если такое число уже есть в контейнере и добавлено оно не было
	 */
	public boolean add(int number) {
		//проверка
		if(has(number)) {
			return false;
		}
		
		//добавление
		numbers[pointer] = number;
		//смещение указателя
		pointer++;
		return true;
	}
	
	/**
	 * Возвращает число, содержащееся в контейнере на <code>index</code> месте
	 * 
	 * @param index позиция числа в контейнере относительно начала строки
	 * @return число, содержащееся в контейнере на <code>index</code> месте
	 */
	public int get(int index) {
		return numbers[index];
	}
	
	/**
	 * Изменяет число, находящееся по индексу index в контейнере,
	 * если такого числа еще нет в контейнере
	 * 
	 * @param index позиция числа, относительно начала
	 * @param number новое значение
	 * @return true, если число добавлено в контейнер, false если такое число уже есть в контейнере и добавлено оно не было
	 */
	public boolean set(int index, int number) {
		if(number > 0 && has(number)) {
			return false;
		}
		
		numbers[index] = number;
		return true;
	}
	
	/**
	 * Возвращает массив, содержащий все числа, добавленные ранее в контейнер
	 * в том порядке, в каком они были добавлены
	 * 
	 * @return массив целых чисел
	 */
	public int[] dump() {
		return Arrays.copyOf(numbers, numbers.length);
	}
	
	/**
	 * Возвращает экземпляр класса String, содержащий
	 * все числа, добавленные ранее в контейнер, отделяемые
	 * друг от друга символом пробела
	 */
	@Override
	public String toString() {
		//объект-соединитель
		StringJoiner joiner = new StringJoiner(" ");
		//перебор
		for(int number : numbers) {
			joiner.add(String.valueOf(number == -1 ? "-" : number));
		}
		//удачи
		return joiner.toString();
	}
}
