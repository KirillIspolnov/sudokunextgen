package org.kirillbogatikoff.sudoku;

import java.util.StringJoiner;

/**
 * Представляет собой карту Судоку - совокупность
 * строк с числами, расположенными на карте
 * особым образом:<br />
 * 
 * каждый столбец и строка не могут содержать повторяющиеся значения<br />
 * К примеру, карта 9х9 содержит 9 блоков. Блок имеет размер 3х3 и также не может содержать
 * повторяющиеся значения<br />
 * Размер блока равен квадратному корню из размера карты судоку
 * 
 * @author Кирилл И., 16ит18К
 * @version 0.1.2
 */
public class Map {
	//строки
	private Part[] rows;
	//указатель, используется при автозаполнении строк
	private int pointer = 1;
	int size;
	int blockSize;
	
	/**
	 * Инициализирует карту указанного размера <code>size</code> 
	 * 
	 * @param size размерность судоку (длина строки или столбца)
	 */
	public Map(int size) {
		this.size = size;
		this.blockSize = (int)Math.sqrt(size);
		rows = new Part[size];
		for(int i = 0; i < size; i++) {
			rows[i] = new Part(size);
		}
	}
	
	/**
	 * Возвращает строку с индексом <code>rowIndex</code>
	 * 
	 * @param rowIndex индекс строки относительно верхнего края карты
	 * @return строка, находящаяся на карте по индексу <code>rowIndex</code>
	 */
	public Part getRow(int rowIndex) {
		return rows[rowIndex];
	}
	
	/**
	 * Возвращает столбец с индексом <code>columnIndex</code>
	 * 
	 * @param columnIndex индекс строки относительно левого края карты
	 * @return столбец, находящийся на карте по индексу <code>columnIndex</code>
	 */
	public Part getColumn(int columnIndex) {
		//новый контейнер
		Part column = new Part(size);
		//перебор всех строк
		for(Part row : rows) {
			column.add(row.get(columnIndex));
		}
		return column;
	}
	
	/**
	 * Возвращает блок, в котором находится ячейка,
	 * расположенная на пересечении строки rowIndex и столбца columnIndex
	 * 
	 * @param rowIndex индекс строки относительно верхнего края карты
	 * @param columnIndex индекс строки относительно левого края карты
	 * @return блок, в котором находится укзаанная ячейка
	 */
	public Part getBlock(int rowIndex, int columnIndex) {
		rowIndex = rowIndex / blockSize;
		columnIndex = columnIndex / blockSize;
		
		Part block = new Part(size);
		
		for(int i = 0; i < blockSize; i++) {
			for(int j = 0; j < blockSize; j++) {
				block.add(getNumber(rowIndex + i, columnIndex + j));
			}
		}
		return block;
	}
	
	/**
	 * Возвращает число, находящееся на пересечении строки row и столбца column
	 * 
	 * @param row индекс строки относительно верхнего края карты
	 * @param column индекс строки относительно левого края карты
	 * @return число, находящееся на пересечении строки row и столбца column 
	 */
	public int getNumber(int row, int column) {
		return getRow(row).get(column);
	}
	
	/**
	 * Заполняет очередную строку таблицы, используя
	 * данные из первой строки и маску
	 * Маска определяет, в каком порядке элементы из
	 * первой строки расположены во всех остальных
	 * строчках. В данном методе используется маска
	 * конкретной строчки, а не карты в целом
	 * 
	 * @param rowMask маска строки
	 */
	public void obtain(int[] rowMask) {
		Part firstRow = getRow(0);
		Part targetRow = getRow(pointer++);
		
		for(int index : rowMask) {
			targetRow.add(firstRow.get(index));
		}
	}
	
	/**
	 * Добавляет новое число на карту,
	 * если такого числа еще нет в контейнере
	 * 
	 * @param rowIndex номер строки
	 * @param columnIndex номер столбца
	 * @param number число
	 * @return true, если число добавлено на карту, false если такое число уже есть в строке/столбце или блоке и добавлено оно не было
	 */
	public boolean set(int rowIndex, int columnIndex, int number) {
		Part row = getRow(rowIndex);
		Part column = getColumn(columnIndex);
		Part block = getBlock(rowIndex, columnIndex);
		
		if(row.has(number) || column.has(number) || block.has(number)) {
			return false;
		}
		
		row.set(columnIndex, number);
		return true;
	}
	
	/**
	 * Возвращает двумерный массив, содержащий все числа, добавленные ранее на карту или строки
	 * в том порядке, в каком они были добавлены
	 * 
	 * @return двумерный массив целых чисел
	 */
	public int[][] dump() {
		int[][] matrix = new int[size][size];
		for(int i = 0; i < size; i++) {
			matrix[i] = rows[i].dump();
		}
		return matrix;
	}
	
	/**
	 * Возвращает экземпляр класса String, содержащий
	 * все числа, добавленные ранее на карту, отделяемые
	 * друг от друга символом пробела. Строки карты отделены
	 * друг от дргуа символом перехода на новую строку '\n'
	 */
	@Override
	public String toString() {
		StringJoiner joiner = new StringJoiner("\n");
		for(Part row : rows) {
			joiner.add(row.toString());
		}
		return joiner.toString();
	}
}	
