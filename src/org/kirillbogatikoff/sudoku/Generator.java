package org.kirillbogatikoff.sudoku;

import java.util.Random;

public class Generator {
	/**
	 * Данное значение установлено в ячейках,
	 * в которых число отстуствует
	 */
	public static final int HOLE = -1;
	/**
	 * Размер судоку 
	 * Другие размеры не поддерживаются, из-за отсутствия
	 * масок
	 */
	public static final int SIZE = 9;
	/**
	 * Маска для судоку 9*9
	 * Маски определяют каким образом по карте
	 * распределены заранее сгенерированные случайно
	 * числа
	 */
	private static final int[][] STANDART_MASK = {
		{ 7, 8, 6, 1, 2, 0, 5, 3, 4 },
		{ 4, 5, 3, 8, 7, 6, 1, 2, 0 },
		{ 2, 7, 0, 4, 1, 8, 3, 6, 5 },
		{ 1, 6, 5, 0, 3, 7, 8, 4, 2 },
		{ 8, 3, 4, 5, 6, 2, 0, 1, 7 },
		{ 3, 0, 1, 2, 5, 4, 7, 8, 6 },
		{ 6, 4, 8, 7, 0, 3, 2, 5, 1 },
		{ 5, 2, 7, 6, 8, 1, 4, 0, 3 }	
	};
	
	/**
	 * Возвращает новую полностью заполненную карту
	 * 
	 * @return карта Судоку, на которой отстутствуют пустые ячейки
	 */
	public Map generate() {
		//контейнер
		Map map = new Map(SIZE);
		
		Random random = new Random();
		int i = 0;
		//генерация первой строки - первых 9 чисел
		while(i < SIZE) {
			//необходимо убедиться что мы сгенерировали все 9 чисел, а не просто
			//попытались 9 раз
			if(map.set(0, i, random.nextInt(SIZE) + 1)) {
				i++;
			}
		}
		
		//нам необходимо обработать 8 строк
		//т.к. первая строка уже сгенерирована ранее
		//и именно из нее значения используются при
		//заполнении других строк
		for(i = 0; i < SIZE - 1; i++) {
			//у каждой строки своя маска
			map.obtain(STANDART_MASK[i]);
		}
		return map;
	}
	
	/**
	 * Возвращает новую карту, в которой
	 * отсутствуют count ячеек
	 * 
	 * Предполагается, что метод {@link #generate()}
	 * используется для получения эталонной карты, 
	 * содержащей правильное решение головоломки
	 * Для получения головоломки (карты с некоторыми пустыми ячейками)
	 * испольузется данный метод
	 * 
	 * Работа метода не влияет на исходные данные
	 * 
	 * @param count количество пустых ячеек
	 * @param map исходная полностью заполненная карта
	 * @return новая карта, в которой отсутствуют count ячеек
	 */
	public Map generateHoles(int count, Map map) {
		if(count / (SIZE * SIZE) >= 5) {
			//Нельзя удалять так много ячеек
			//Пока не найдено Судоку, имеющая 65 пустых ячеек и при этом
			//единственное верное решение
			throw new RuntimeException("Too many holes: " + count + "/" + (SIZE * SIZE) + "!");
		}
		
		Map copy = new Map(SIZE);
		for(int i = 0; i < SIZE; i++) {
			copy.set(0, i, map.getRow(i).get(i));
		}
		for(int i = 0; i < SIZE - 1; i++) {
			copy.obtain(STANDART_MASK[i]);
		}
		
		Random random = new Random();
		//кол-во пустот на одной строке
		int holesPerLine = count / SIZE;
		
		for(int i = 0; i < SIZE; i++) {
			//кол-во пустот на этой строке
			int c = 0;
			for(int j = 0; j < SIZE; j++) {
				//на все воля случая
				if(random.nextBoolean()) {
					//удаляем значение
					map.getRow(i).set(j, HOLE);
					
					//если в этой строке уже сделано
					//максимально возможное кол-во пустых
					//ячеек, необходимо перейти к следующей строке
					if(++c >= holesPerLine) {
						break;
					}
				}
			}
		}
		return map;
	}
}
